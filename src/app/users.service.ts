import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';


export interface User {
  gender: string;
  name: string;
  picture: string;
  location: string;
}




@Injectable({
  providedIn: 'root'
})
export class UsersService {

users: User[] = [];

// usersUrl =
// 	 'https://randomuser.me/api/?inc=gender,name,picture,location&results=6&nat=gb';

usersUrl = '/assets/users.json';
  

  constructor(
	private http: HttpClient
  	) { }


   
  
  getUsers() {
    var pesult = this.http.get(this.usersUrl);


  	console.log(pesult);
  	return pesult;
  }

  // getUsersAll(){
   
  //     this.http.get(this.usersUrl).subscribe(
  //     (data: User) => { this.users = data['results'];
  //                      // console.log(this.users);
  //   });
       
  // }

  // gerMyList(){
  //   return this.users; 
  // }

  oneUser(userId){
    return this.users[userId];
  }

 } 

