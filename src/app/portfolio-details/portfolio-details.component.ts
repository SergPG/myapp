import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Input } from '@angular/core';

import { User, UsersService } from '../users.service';
//import { PortfolioSectionComponent } from '../portfolio-section/portfolio-section.component';
//import { User, UsersService } from '../users.service';

@Component({
  selector: 'app-portfolio-details',
  templateUrl: './portfolio-details.component.html',
  styleUrls: ['./portfolio-details.component.css']
})
export class PortfolioDetailsComponent implements OnInit {

	// @Input() users ;
  // @Input() userId ;
  users: User[]=[];
  user: User;
  
 // users;
 // user ;
  params;

  constructor(
    private route: ActivatedRoute,
    private usersService:UsersService
  	) { }

  ngOnInit() {
     
     this.route.paramMap.subscribe(params => { this.params = params.get('userId');
          console.log(this.params);
      });


      // this.users = this.usersService.gerMyList();

      // console.log(this.users);

     this.usersService.getUsers()
          .subscribe(
              (data: User) => { 
                this.users = data['results'];
               // this.usersService.users = this.users;
                this.user = this.users[this.params];
               console.log(this.users);
              });



  	// this.route.paramMap.subscribe(params => {
          
   //        this.user = this.usersService.oneUser(params.get('userId'));
   //        console.log(this.user);
   //    });

   

    // console.log(this.params);
    // console.log(this.user);
  }

}
