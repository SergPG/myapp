import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { MarketingService } from '../marketing.service';

@Component({
  selector: 'app-marketing-details',
  templateUrl: './marketing-details.component.html',
  styleUrls: ['./marketing-details.component.css']
})
export class MarketingDetailsComponent implements OnInit {

	marketing;

  constructor(
    private route: ActivatedRoute,
    private marketingService: MarketingService
  	) { }

  ngOnInit() {
  	this.route.paramMap.subscribe(params => {
    this.marketing = params.get('id');
  });
  }

}
