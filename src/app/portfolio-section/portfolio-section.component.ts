import { Component, OnInit } from '@angular/core';
import { User, UsersService } from '../users.service';

@Component({
  selector: 'app-portfolio-section',
  templateUrl: './portfolio-section.component.html',
  styleUrls: ['./portfolio-section.component.css']
})
export class PortfolioSectionComponent implements OnInit {

	users: User[]=[];

  constructor(
     private usersService:UsersService
  	) { 


  }

  ngOnInit() {

      /*this.usersService.getUsersAll();*/

      this.usersService.getUsers()
    .subscribe(
      (data: User) => { 
        this.users = data['results'];
       // this.usersService.users = this.users;
        console.log(this.users);
      });

  	


     


    //  this.users = this.usersService.gerMyList();
     


   } 

  // getUsersAll() {
  //   console.log(this.users);
  //   return this.users;
  // }

}
