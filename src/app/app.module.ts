import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import {NgbModule} from '@ng-bootstrap/ng-bootstrap';

import { AppRoutingModule } from './app-routing.module';
import { RouterModule } from '@angular/router';
import { HttpClientModule } from '@angular/common/http';


import { AppComponent } from './app.component';
import { TopBarComponent } from './top-bar/top-bar.component';
import { PageHomeComponent } from './page-home/page-home.component';
import { HeaderComponent } from './header/header.component';
import { FooterComponent } from './footer/footer.component';
import { CardMarketingComponent } from './card-marketing/card-marketing.component';
import { CardImageComponent } from './card-image/card-image.component';
import { MarketingSectionComponent } from './marketing-section/marketing-section.component';
import { PortfolioSectionComponent } from './portfolio-section/portfolio-section.component';
import { FeaturesSectionComponent } from './features-section/features-section.component';
import { ActionSectionComponent } from './action-section/action-section.component';

import { MarketingService } from './marketing.service';
import { UsersService } from './users.service';
import { MarketingDetailsComponent } from './marketing-details/marketing-details.component';
import { PortfolioDetailsComponent } from './portfolio-details/portfolio-details.component';


@NgModule({
  declarations: [
    AppComponent,
    TopBarComponent,
    PageHomeComponent,
    HeaderComponent,
    FooterComponent,
    CardMarketingComponent,
    CardImageComponent,
    MarketingSectionComponent,
    PortfolioSectionComponent,
    FeaturesSectionComponent,
    ActionSectionComponent,
    MarketingDetailsComponent,
    PortfolioDetailsComponent,
    
  ],
  imports: [
    BrowserModule,
    NgbModule,
    HttpClientModule,
    AppRoutingModule,
    RouterModule.forRoot([
      { path: '', component: PageHomeComponent },
      { path: 'marketing/:id', component: MarketingDetailsComponent },
      { path: 'users/:userId', component: PortfolioDetailsComponent },
      
    ])
  ],
  providers: [MarketingService,
              UsersService],
  bootstrap: [AppComponent]
})
export class AppModule { }
