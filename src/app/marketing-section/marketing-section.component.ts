import { Component, OnInit } from '@angular/core';
import { MarketingService } from '../marketing.service';



@Component({
  selector: 'app-marketing-section',
  templateUrl: './marketing-section.component.html',
  styleUrls: ['./marketing-section.component.css']
})
export class MarketingSectionComponent implements OnInit {

	marketings;

  constructor(
    private marketingService: MarketingService
  	) {
         this.marketings = this.marketingService.getMarketing();
  	 }

  ngOnInit() {
  }

}
