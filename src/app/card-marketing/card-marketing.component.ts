import { Component, OnInit } from '@angular/core';
import { Input } from '@angular/core';

@Component({
  selector: 'app-card-marketing',
  templateUrl: './card-marketing.component.html',
  styleUrls: ['./card-marketing.component.css']
})
export class CardMarketingComponent implements OnInit {

  	 @Input() marketing;

  constructor() { }

  ngOnInit() {
  }

}
