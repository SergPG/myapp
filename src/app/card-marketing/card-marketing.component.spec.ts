import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CardMarketingComponent } from './card-marketing.component';

describe('CardMarketingComponent', () => {
  let component: CardMarketingComponent;
  let fixture: ComponentFixture<CardMarketingComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CardMarketingComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CardMarketingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
